# Short script to delete pages from a PDF

import sys
import pikepdf
from pathlib import Path

delPageList = [x for x in sys.argv[1].split(",")]
inFile = Path(sys.argv[2])
outFile = Path(sys.argv[3])

print(f"Deleting pages {', '.join(delPageList)} from {inFile}, writing to {outFile}")
delPageList = [int(x) - 1 for x in delPageList]

with pikepdf.Pdf.open(inFile) as pdf:
    newPdf = pikepdf.Pdf.new()
    for index, page in enumerate(pdf.pages):
        if index not in delPageList:
            newPdf.pages.append(page)
    newPdf.save(outFile)

print("Success!")
