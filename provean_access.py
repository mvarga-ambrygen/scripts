"""
This script provides an example of how to access the internal Provean websocket API, using MLH1 and several example variants
"""

import asyncio, json
import websockets
from collections import Counter
from math import nan
from uuid import uuid4

proveanAPI = "ws://provean-api-test/ws/json/watch"


async def get_provean_scores(sequence, variants):
    sessionId = str(uuid4())
    payload = {"uid": sessionId, "elements": [{"sequence": sequence, "variants": variants}]}
    varList = Counter(variants)
    results = {}
    try:
        async with websockets.connect(proveanAPI) as host:
            await host.send(json.dumps(payload))
            message = await asyncio.wait_for(host.recv(), timeout=30)
            while True:
                message = await asyncio.wait_for(host.recv(), timeout=1800)
                message = json.loads(message)
                if message.get("transition") == "ERROR":
                    print(message)
                    await host.close()
                    break
                elif message.get("transition") == "UPDATE":
                    for elem in message["contents"]:
                        if "score" in elem and elem["paths"][0][0] == sessionId:
                            results[elem["paths"][0][-1]] = float(elem["score"])
                        elif elem.get("error", False):
                            results[elem["paths"][0][-1]] = nan
                            print(f"Error calculating score for {elem['paths'][0][-1]}")
                    print(f"{len(results.keys())}/{len(varList.keys())}")
                    if varList == Counter(results.keys()):
                        await host.close()
                        break
            print("The job has completed.")
    except asyncio.TimeoutError:
        print("Error: Timeout")
        return results
    return results


sequence = (
    "MSFVAGVIRRLDETVVNRIAAGEVIQRPANAIKEMIENCLDAKSTSIQVIVKEGGLKLIQIQDNGTGIRKE"
    "DLDIVCERFTTSKLQSFEDLASISTYGFRGEALASISHVAHVTITTKTADGKCAYRASYSDGKLKAPPKPC"
    "AGNQGTQITVEDLFYNIATRRKALKNPSEEYGKILEVVGRYSVHNAGISFSVKKQGETVADVRTLPNASTV"
    "DNIRSIFGNAVSRELIEIGCEDKTLAFKMNGYISNANYSVKKCIFLLFINHRLVESTSLRKAIETVYAAYL"
    "PKNTHPFLYLSLEISPQNVDVNVHPTKHEVHFLHEESILERVQQHIESKLLGSNSSRMYFTQTLLPGLAGP"
    "SGEMVKSTTSLTSSSTSGSSDKVYAHQMVRTDSREQKLDAFLQPLSKPLSSQPQAIVTEDKTDISSGRARQ"
    "QDEEMLELPAPAEVAAKNQSLEGDTTKGTSEMSEKRGPTSSNPRKRHREDSDVEMVEDDSRKEMTAACTPR"
    "RRIINLTSVLSLQEEINEQGHEVLREMLHNHSFVGCVNPQWALAQHQTKLYLLNTTKLSEELFYQILIYDF"
    "ANFGVLRLSEPAPLFDLAMLALDSPESGWTEEDGPKEGLAEYIVEFLKKKAEMLADYFSLEIDEEGNLIGL"
    "PLLIDNYVPPLEGLPIFILRLATEVNWDEEKECFESLSKECAMFYSIRKQYISEESTLSGQQSEVPGSIPN"
    "SWKWTVEHIVYKALRSHILPPKHFTEDGNILQLANLPDLYKVFERC"
)

variants = ["A21del", "A541P", "A586D", "C39R", "L177_G181del", "R474G", "S577L"]

results = asyncio.get_event_loop().run_until_complete(get_provean_scores(sequence, variants))
