#!/usr/bin/env python

"""
This script creates figures of BayesDel scores for proteins positions, along with graphics of protein domains in the AVA3 database.
Requires a '.env' file in the working directory containing two variables: 'USERNAME_X14' and 'PASSWORD_X14', credentials for the
usav1svsqlt14 database

|-------------------|
|  protein domains  |
|-------------------|---|
|                   |   |
|                   | P |
|  BayesDel scores  | D |
|                   | F |
|___________________|___|

"""

import ast
import re
import pickle
import click
import requests
import numpy as np
import plotly.graph_objects as go
import logging
import pandas as pd
from dotenv import load_env
from math import ceil
from textwrap import wrap
from plotly.subplots import make_subplots
from plotly.colors import qualitative

from pandas import DataFrame
from json import JSONDecodeError
from sqlalchemy import text, create_engine
from urllib.parse import quote_plus

load_env()

logging.basicConfig(level=logging.INFO)

_INSIDE_HIST_COLOR = "salmon"
_OUTSIDE_HIST_COLOR = "gray"
_HIST_OPACITY = 0.3
_BIN_SIZE = 0.025


def normalize(val, vMin, vMax) -> float:
    return (val - vMin) / (vMax - vMin)


def rounded_box_path(x0, x1, y0, y1) -> str:
    h = 7
    rounded_bottom_left = f" M {x0 + h}, {y0} Q {x0}, {y0} {x0}, {y0 + h}"
    rounded_top_left = f" L {x0}, {y1 - h} Q {x0}, {y1} {x0 + h}, {y1}"
    rounded_top_right = f" L {x1 - h}, {y1} Q {x1}, {y1} {x1}, {y1 - h}"
    rounded_bottom_right = f" L {x1}, {y0 + h} Q {x1}, {y0} {x1 - h}, {y0}Z"
    return rounded_bottom_left + rounded_top_left + rounded_top_right + rounded_bottom_right


def get_protein_structure(gene):
    _url = "http://api/api/alterations/prot_struct.json?protein_id={npid}"
    try:
        response = requests.get(_url.format(npid=gene["protein_id"])).json()["prot_parameter"]
    except JSONDecodeError:
        return None
    for feature in response:
        try:
            for key in ("start", "stop"):
                feature[key] = int(feature[key])
                feature[f"cds_{key}"] = feature[key] * 3
                feature[f"gene_{key}"] = gene["cds_to_gene"][feature[f"cds_{key}"]]
            if "domain_name" not in feature:
                feature["domain_name"] = feature["name"]
            if "description" not in feature:
                feature["description"] = feature["domain_name"]
            if "type" not in feature:
                feature["type"] = "domain"
        except KeyError:
            logging.warning(f"Could not parse feature...")
    return response


def get_transcript_info(isoform):
    try:
        response = requests.get(
            f"http://pyapi/api/v1.0/info/transcript?nucleotide_id={isoform}"
        ).json()["api_data"][0]
        for key in ("cds_structure", "mrna_structure"):
            response[key] = ast.literal_eval(response[key])
            if response["orientation"] == "-":
                response[key] = [list(reversed(exon)) for exon in reversed(response[key])]
        response["cds_numbering"] = [
            [1, 1 + abs(response["cds_structure"][0][1] - response["cds_structure"][0][0])]
        ]
        for i in range(1, len(response["cds_structure"])):
            exonStart = 1 + response["cds_numbering"][i - 1][1]
            exonStop = exonStart + abs(
                response["cds_structure"][i][1] - response["cds_structure"][i][0]
            )
            response["cds_numbering"].append([exonStart, exonStop])
        cdsRanges = [
            list(np.arange(response["cds_numbering"][i][0], response["cds_numbering"][i][1] + 1))
            for i in range(0, len(response["cds_numbering"]))
        ]
        cdsRanges = [elem for exon in cdsRanges for elem in exon]
        if response["orientation"] == "+":
            geneRanges = [
                list(
                    np.arange(response["cds_structure"][i][0], response["cds_structure"][i][1] + 1)
                )
                for i in range(0, len(response["cds_structure"]))
            ]
        else:
            geneRanges = [
                list(
                    reversed(
                        np.arange(
                            response["cds_structure"][i][1], response["cds_structure"][i][0] + 1
                        )
                    )
                )
                for i in range(0, len(response["cds_structure"]))
            ]
        geneRanges = [elem for exon in geneRanges for elem in exon]
        response["cds_to_gene"] = dict(list(zip(cdsRanges, geneRanges)))
        response["gene_to_cds"] = dict(list(zip(geneRanges, cdsRanges)))
        return {
            **{
                key: response.get(key)
                for key in (
                    "chromosome",
                    "orientation",
                    "protein_id",
                    "cds_structure",
                    "mrna_structure",
                    "cds_numbering",
                    "cds_to_gene",
                    "gene_to_cds",
                    "symbol",
                    "nucleotide_id",
                    "protein_length",
                )
            },
            "prot_features": get_protein_structure(response),
        }
    except (JSONDecodeError, KeyError):
        return None


def get_chromosome_bayesdel_scores(gene, dbEngine):
    chrNum = gene["chromosome"]
    query = text(
        f"""SELECT
    statistical_genetics.BayesDel_noallelefrequency_chr{chrNum}.start,
        statistical_genetics.BayesDel_noallelefrequency_chr{chrNum}.ref,
        statistical_genetics.BayesDel_noallelefrequency_chr{chrNum}.alt,
        statistical_genetics.BayesDel_noallelefrequency_chr{chrNum}.score
    FROM statistical_genetics.BayesDel_noallelefrequency_chr{chrNum}
    WHERE start BETWEEN :start AND :stop"""
    )
    with dbEngine.connect() as connection:
        if gene["orientation"] == "+":
            params = dict(start=gene["cds_structure"][0][0], stop=gene["cds_structure"][-1][1])
        else:
            params = dict(start=gene["cds_structure"][-1][1], stop=gene["cds_structure"][0][0])
        result = connection.execute(query, params).fetchall()
    return [dict(elem) for elem in result]


def get_bayesdel_thresholds(gene, dbEngine):
    query = text(
        """SELECT
            statistical_genetics.bayesdel_thresholds.lower_threshold,
            statistical_genetics.bayesdel_thresholds.upper_threshold
        FROM statistical_genetics.bayesdel_thresholds
        WHERE statistical_genetics.bayesdel_thresholds.gene = :symbol
            OR statistical_genetics.bayesdel_thresholds.gene = 'others'"""
    )
    with dbEngine.connect() as connection:
        result = connection.execute(query, {"symbol": gene["symbol"]}).first()
    return [float(x) for x in result]


def get_patient_variants(gene, onlyMissense: bool = True, includeVUS: bool = False):
    logging.info("Getting patient variants...")
    ntTable = {"A": "T", "T": "A", "C": "G", "G": "C"}
    _regex = re.compile(r"\bc.(\d+)([A-Z])>([A-Z])\b", re.IGNORECASE)
    _missenseCheck = re.compile(r"\bp.[A-Z]\d+[A-Z]\b", re.IGNORECASE)
    results = requests.get(
        f"http://ava-api.ambrygen.com/api/bio/patients/{gene['protein_id']}"
    ).json()
    out = []
    for result in results:
        try:
            if onlyMissense:
                _missenseCheck.match(result["p_variant"]).group()
            if not includeVUS and result["category_name"] == "VUS":
                continue
            match = _regex.search(result["c_variant"])
            result["alt"] = (
                match.group(3) if gene["orientation"] == "+" else ntTable[match.group(3)]
            )
            result["ref"] = (
                match.group(2) if gene["orientation"] == "+" else ntTable[match.group(2)]
            )
            result["pos"] = match.group(1)
            out.append(result)
        except AttributeError as e:
            continue
    data = DataFrame.from_dict(out)
    data["ref"] = data["ref"].astype("string")
    data["alt"] = data["alt"].astype("string")
    data["pos"] = data["pos"].astype("int64")
    return data


def annotate_scores_with_domains(gene, scores, **kwargs):
    domainRange = set()
    for feature in gene["prot_features"]:
        if feature["type"] == "domain":
            domainRange.update(range(feature["cds_start"], feature["cds_stop"]))
    values = []
    for score in scores:
        if score["start"] in gene["gene_to_cds"].keys():
            values.append(
                [
                    gene["gene_to_cds"][score["start"]],
                    score["ref"],
                    score["alt"],
                    float(score["score"]),
                    gene["gene_to_cds"][score["start"]] in domainRange,
                ]
            )

    data = DataFrame(values, columns=["pos", "ref", "alt", "score", "inDomain"], index=None)
    data["ref"] = data["ref"].astype("string")
    data["alt"] = data["alt"].astype("string")
    data["pos"] = data["pos"].astype("int64")
    return data


_defaultFontFamily = "Helvetica"
_plotOutlineParams = dict(showline=True, linewidth=1, linecolor="lightgray", mirror=True)
tickFont = dict(family=_defaultFontFamily, size=12)
axisFont = dict(family=_defaultFontFamily, size=12)
titleFont = dict(family=_defaultFontFamily, size=24)
annotationFont = dict(family=_defaultFontFamily, size=10)


def make_plot(lowThresh, upThresh, gene, scores, stackHistogram: bool = False):
    maxVal = 0.743871
    minVal = -1.55016
    yMin = -1.0
    yMax = 0.8
    xMax = gene["protein_length"] * 3

    df = annotate_scores_with_domains(gene, scores)

    fig = make_subplots(
        rows=6,
        cols=4,
        specs=[
            [{"colspan": 3}, None, None, None],
            [{"rowspan": 5, "colspan": 3}, None, None, {"rowspan": 5}],
            [None, None, None, None],
            [None, None, None, None],
            [None, None, None, None],
            [None, None, None, None],
        ],
        vertical_spacing=0.001,
        horizontal_spacing=0.005,
    )
    fig.add_trace(
        go.Scatter(
            x=df["pos"],
            y=df["score"],
            name="scores",
            mode="markers",
            marker=dict(size=5, color=df["score"], coloraxis="coloraxis1"),
            hovertemplate="<b>Alteration:</b> %{x}%{text}<br>" + "<b>BayesDel Score:</b> %{y:.3f}",
            text=[df.iloc[i]["ref"] + ">" + df.iloc[i]["alt"] for i in range(0, len(df))],
        ),
        row=2,
        col=1,
    )
    fig.update_xaxes(
        row=2,
        col=1,
        title=dict(text="Position", font=axisFont),
        tickfont=tickFont,
        range=[0, xMax],
        showline=True,
        linewidth=1,
        linecolor="gray",
        mirror=True,
    )
    fig.update_yaxes(
        row=2,
        col=1,
        title=dict(text="BayesDel Score", font=axisFont),
        range=[yMin, yMax],
        tickvals=[yMin, -1, -0.5, yMax],
        tickfont=tickFont,
        showline=True,
        linewidth=1,
        linecolor="gray",
        mirror=True,
    )
    for thresh in (lowThresh, upThresh):
        fig.add_shape(
            type="line",
            x0=0,
            x1=xMax,
            y0=thresh,
            y1=thresh,
            line=dict(width=1, color="gray", dash="dash"),
            xref="x2",
            yref="y2",
        )
        fig.add_annotation(
            text=f"<i>{thresh}</i>",
            x=0,
            y=thresh,
            font=tickFont,
            xref="paper",
            yref="y2",
            showarrow=False,
            yanchor="middle",
            xanchor="right",
            xshift=-3,
        )
    fig.update_layout(
        coloraxis=dict(
            cmin=minVal,
            cmax=maxVal,
            colorscale=[
                (0, "#5AE655"),
                (normalize(lowThresh, minVal, maxVal), "#5AE655"),
                (normalize(lowThresh, minVal, maxVal), "#DEE558"),
                (normalize(upThresh, minVal, maxVal), "#DEE558"),
                (normalize(upThresh, minVal, maxVal), "#E44A50"),
                (1, "#E44A50"),
            ],
            showscale=False,
        )
    )

    if stackHistogram:
        fig.add_trace(
            go.Histogram(
                y=df[df["inDomain"] == False]["score"],
                name="outside domains",
                # histnorm="probability",
                marker=dict(color="gray"),
                opacity=0.5,
            ),
            row=2,
            col=4,
        )
        fig.add_trace(
            go.Histogram(
                y=df[df["inDomain"] == True]["score"],
                name="within domains",
                # histnorm="probability",
                marker=dict(color="salmon"),
                opacity=0.5,
            ),
            row=2,
            col=4,
        )
        fig.add_annotation(
            text="inside a protein domain",
            font=dict(color="salmon", **annotationFont),
            x=0,
            y=-1.59,
            xref="x3",
            yref="y3",
            xanchor="left",
            yanchor="bottom",
            showarrow=False,
        )
        fig.add_annotation(
            text="outside a protein domain",
            font=dict(color="gray", **annotationFont),
            x=0,
            y=-1.58,
            xref="x3",
            yref="y3",
            xanchor="left",
            yanchor="top",
            showarrow=False,
        )
        fig.update_layout(barmode="stack")
    else:
        fig.add_trace(
            go.Histogram(
                y=df["score"],
                name="outside domains",
                histnorm="probability",
                marker=dict(color="gray"),
                opacity=0.5,
            ),
            row=2,
            col=4,
        )
    fig.update_yaxes(
        row=2,
        col=4,
        range=[yMin, yMax],
        showticklabels=False,
        showline=True,
        linewidth=1,
        linecolor="gray",
        showgrid=False,
        tickfont=tickFont,
        title=dict(font=axisFont),
    )
    fig.update_xaxes(
        row=2,
        col=4,
        showticklabels=False,
        showgrid=False,
        zeroline=False,
        tickfont=tickFont,
        title=dict(font=axisFont),
    )

    colors = iter(qualitative.Light24_r)
    # protein baseline
    fig.add_shape(x0=1, y0=-1, x1=xMax, y1=1, row=1, col=1, fillcolor="LightGray")
    for feature in gene["prot_features"]:
        if feature["type"] == "domain":
            fig.add_shape(
                type="path",
                path=rounded_box_path(
                    x0=feature["cds_start"],
                    x1=feature["cds_stop"],
                    y0=-10,
                    y1=10,
                ),
                fillcolor=next(colors),
                row=1,
                col=1,
            )
            fig.add_annotation(
                text="<br>".join(wrap(feature["domain_name"], 20)),
                x=feature["cds_start"] + (feature["cds_stop"] - feature["cds_start"]) / 2,
                y=11,
                showarrow=False,
                yanchor="bottom",
                hovertext=feature["description"],
                textangle=-45,
                font=annotationFont,
            )
    fig.update_yaxes(
        row=1, col=1, range=[-20, 30], showticklabels=False, showgrid=False, zeroline=False
    )
    fig.update_xaxes(
        row=1, col=1, range=[0, xMax], showticklabels=False, showgrid=False, zeroline=False
    )

    fig.update_layout(
        template="plotly_white",
        showlegend=False,
        width=1400,
        height=900,
        title=dict(
            text=f"{gene['symbol']} ({gene['nucleotide_id']} | {gene['protein_id']})",
            font=titleFont,
        ),
    )

    fig.write_image(f"{gene['symbol']}_{gene['nucleotide_id']}_bayesdel.png")


def make_plot_with_variants(lowThresh, upThresh, gene, scores):
    maxVal = 0.743871
    minVal = -1.55016
    yMin = -1.0
    yMax = 0.8
    xMax = gene["protein_length"] * 3

    colourMap = {
        "Mutation": "salmon",
        "VLP": "orange",
        "VUS": "gainsboro",
        "VLB": "mediumseagreen",
        "Poly": "mediumslateblue",
    }

    data = annotate_scores_with_domains(gene, scores)
    variants = get_patient_variants(gene)
    assessedData = data.merge(variants, on=["pos", "alt", "ref"]).sort_values(by="category_updated")
    otherData = data.merge(variants, on=["pos", "alt", "ref"], how="left")
    otherData = otherData[otherData["category_name"].isna()]

    fig = make_subplots(
        rows=6,
        cols=4,
        specs=[
            [{"colspan": 3}, None, None, None],
            [{"rowspan": 5, "colspan": 3}, None, None, {"rowspan": 5}],
            [None, None, None, None],
            [None, None, None, None],
            [None, None, None, None],
            [None, None, None, None],
        ],
        vertical_spacing=0.001,
        horizontal_spacing=0.005,
    )
    colours = [colourMap[x] for x in assessedData["category_name"]]
    fig.add_trace(
        go.Scatter(
            x=otherData["pos"],
            y=otherData["score"],
            mode="markers",
            marker=dict(size=4, color="lightgray"),
            hovertemplate="<b>Alteration:</b> %{x}%{text}<br>" + "<b>BayesDel Score:</b> %{y:.3f}",
            text=[
                otherData.iloc[i]["ref"] + ">" + otherData.iloc[i]["alt"]
                for i in range(0, len(otherData))
            ],
            opacity=0.3,
            showlegend=False,
        ),
        row=2,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=assessedData["pos"],
            y=assessedData["score"],
            mode="markers",
            marker=dict(size=5, color=colours),
            hovertemplate="%{text}",
            text=[
                f"Alteration: {assessedData.iloc[i]['c_variant']}<br>BayesDel score: {assessedData.iloc[i]['score']}<br>Classification: {assessedData.iloc[i]['category_name']}"
                for i in range(0, len(assessedData))
            ],
            showlegend=False,
        ),
        row=2,
        col=1,
    )
    fig.update_xaxes(
        row=2,
        col=1,
        title=dict(text="Position", font=axisFont),
        tickfont=tickFont,
        range=[0, xMax],
        showline=True,
        linewidth=1,
        linecolor="gray",
        mirror=True,
    )
    fig.update_yaxes(
        row=2,
        col=1,
        title=dict(text="BayesDel Score", font=axisFont),
        range=[yMin, yMax],
        tickvals=[yMin, -1, -0.5, yMax],
        tickfont=tickFont,
        showline=True,
        linewidth=1,
        linecolor="gray",
        mirror=True,
    )
    for thresh in (lowThresh, upThresh):
        fig.add_shape(
            type="line",
            x0=0,
            x1=xMax,
            y0=thresh,
            y1=thresh,
            line=dict(width=1, color="gray", dash="dash"),
            xref="x2",
            yref="y2",
        )
        fig.add_annotation(
            text=f"<i>{thresh}</i>",
            x=0,
            y=thresh,
            font=tickFont,
            xref="paper",
            yref="y2",
            showarrow=False,
            yanchor="middle",
            xanchor="right",
            xshift=-3,
        )

    for classification in assessedData["category_name"].drop_duplicates().to_list():
        fig.add_trace(
            go.Box(
                y=assessedData[assessedData["category_name"] == classification]["score"],
                marker=dict(color=colourMap[classification]),
                name=classification,
            ),
            row=2,
            col=4,
        )
    fig.update_yaxes(
        row=2,
        col=4,
        range=[yMin, yMax],
        showticklabels=False,
        showgrid=False,
        tickfont=tickFont,
        title=dict(font=axisFont),
        zeroline=False,
    )
    fig.update_xaxes(
        row=2,
        col=4,
        showticklabels=False,
        showgrid=False,
        zeroline=False,
        tickfont=tickFont,
        title=dict(font=axisFont),
    )

    colors = iter(qualitative.Light24_r)
    fig.add_shape(x0=1, y0=-1, x1=xMax, y1=1, row=1, col=1, fillcolor="LightGray")
    for feature in gene["prot_features"]:
        if feature["type"] == "domain":
            fig.add_shape(
                type="path",
                path=rounded_box_path(
                    x0=feature["cds_start"],
                    x1=feature["cds_stop"],
                    y0=-10,
                    y1=10,
                ),
                fillcolor=next(colors),
                row=1,
                col=1,
            )
            fig.add_annotation(
                text="<br>".join(wrap(feature["domain_name"], 20)),
                x=feature["cds_start"] + (feature["cds_stop"] - feature["cds_start"]) / 2,
                y=11,
                showarrow=False,
                yanchor="bottom",
                hovertext=feature["description"],
                textangle=-45,
                font=annotationFont,
            )
    fig.update_yaxes(
        row=1, col=1, range=[-20, 30], showticklabels=False, showgrid=False, zeroline=False
    )
    fig.update_xaxes(
        row=1,
        col=1,
        range=[0, xMax],
        showticklabels=False,
        showgrid=False,
        zeroline=False,
    )

    fig.update_layout(
        template="plotly_white",
        width=1400,
        height=900,
        title=dict(
            text=f"{gene['symbol']} ({gene['nucleotide_id']} | {gene['protein_id']})",
            font=titleFont,
        ),
        legend=dict(
            title=dict(text="Ambry Classification", font=axisFont, side="top"),
            font=axisFont,
            yanchor="top",
            xanchor="right",
            x=1,
            y=1,
        ),
    )
    filename = f"{gene['symbol']}_{gene['nucleotide_id']}_bayesdel_w-variants"
    fig.write_image(f"{filename}.png")
    fig.write_html(f"{filename}.html")


def make_histogram_adjacent_correlation_plot(
    data,
    *,
    fig=None,
    row=1,
    col1=None,
    col2=None,
    minScore=-1.0,
    maxScore=0.8,
    overwriteCorrLimits: bool = True,
):
    points = []
    for elem in data:
        tmp1 = elem["df"][elem["df"]["inDomain"] == True]["score"]
        inMean = tmp1.mean()
        inStd = tmp1.std()
        tmp2 = elem["df"][elem["df"]["inDomain"] != True]["score"]
        outMean = tmp2.mean()
        outStd = tmp2.std()
        points.append(
            [
                outMean,
                inMean,
                outStd,
                inStd,
                f"{elem['gene']['symbol']} ({elem['gene']['nucleotide_id']})",
            ]
        )
    x, y, xErr, yErr, labels = list(zip(*points))
    totDF = pd.concat([elem["df"] for elem in data])

    xMin, xMax = yMin, yMax = minScore, maxScore
    if overwriteCorrLimits:
        corrMin = min(x + y)
        corrMin = corrMin + (corrMin * 0.20)
        corrMax = max(x + y)
        corrMax = corrMax + (corrMax * 0.20)
    else:
        corrMin = xMin
        corrMax = xMax
    fig.add_trace(
        go.Scatter(
            x=x,
            y=y,
            mode="markers",
            marker=dict(size=6, opacity=0.5, color="lightgray", line=dict(width=2, color="gray")),
            showlegend=False,
            text=labels,
            hovertemplate=(
                "<b>%{text}</b><br>"
                "<i>Outside domain mean:</i> %{x:.3f}<br>"
                "<i>Inside domain mean:</i> %{y:.3f}"
            ),
        ),
        row=row,
        col=col2,
    )
    fig.update_xaxes(
        row=row,
        col=col2,
        range=[corrMin, corrMax],
        nticks=4,
        title=dict(text="Mean of BayesDel scores at positions outside domains", font=axisFont),
        showgrid=False,
        tickfont=tickFont,
        zeroline=False,
        **_plotOutlineParams,
    ),
    fig.update_yaxes(
        row=row,
        col=col2,
        range=[corrMin, corrMax],
        nticks=4,
        title=dict(text="Mean of BayesDel scores at positions inside domains", font=axisFont),
        tickfont=tickFont,
        showgrid=False,
        zeroline=False,
        **_plotOutlineParams,
    )
    fig.add_shape(
        type="line",
        x0=corrMin,
        x1=corrMax,
        y0=corrMin,
        y1=corrMax,
        line=dict(width=0.5),
        row=row,
        col=col2,
    )
    for thresh in (-0.011, 0.315):
        fig.add_vline(
            x=thresh,
            line=dict(width=0.5, dash="dot"),
            annotation_text=f"<i>{thresh}</i>",
            annotation_position="bottom left",
            annotation=dict(textangle=-90, font=annotationFont),
            row=row,
            col=col2,
        )
        fig.add_hline(
            y=thresh,
            line=dict(width=0.5, dash="dot"),
            annotation_text=f"<i>{thresh}</i>",
            annotation_position="top left",
            annotation=dict(font=annotationFont),
            row=row,
            col=col2,
        )
    fig.add_trace(
        go.Histogram(
            xbins=dict(size=_BIN_SIZE),
            x=totDF[totDF["inDomain"] == False]["score"],
            opacity=_HIST_OPACITY,
            marker=dict(color=_OUTSIDE_HIST_COLOR),
            name="Positions outside a domain",
        ),
        row=row,
        col=col1,
    )
    fig.add_trace(
        go.Histogram(
            xbins=dict(size=_BIN_SIZE),
            x=totDF[totDF["inDomain"] == True]["score"],
            opacity=_HIST_OPACITY,
            marker=dict(color=_INSIDE_HIST_COLOR),
            name="Positions inside a domain",
        ),
        row=row,
        col=col1,
    )
    for thresh in (-0.011, 0.315):
        fig.add_vline(
            x=thresh,
            line=dict(width=0.5, dash="dot"),
            annotation_text=f"<i>{thresh}</i>",
            annotation_position="top left",
            annotation=dict(textangle=-90, font=annotationFont),
            row=row,
            col=col1,
        )
    fig.update_xaxes(
        row=row,
        col=col1,
        range=[xMin, xMax],
        tickvals=[xMin, -0.5, 0, 0.5, xMax],
        title=dict(text="BayesDel score", font=axisFont),
        tickfont=tickFont,
        showgrid=False,
        **_plotOutlineParams,
    )
    fig.update_yaxes(row=row, col=col1, showgrid=False, **_plotOutlineParams)


def make_histogram_matrix(data, title, filterGenes: bool = False, outFilename="all_genes_matrix"):
    dataset = []
    for elem in data:
        if "df" not in elem:
            elem["df"] = annotate_scores_with_domains(**elem)
        tmp = elem["df"]["inDomain"].value_counts(normalize=True)
        if True in tmp.index:
            # if filterGenes and not 0.80 >= tmp[True] >= 0.20:
            #     continue
            dataset.append(elem)
    dataset = sorted(dataset, key=lambda x: x["gene"]["symbol"])

    numCols = 6
    numRows = ceil(len(dataset) / numCols) + 2
    fig = make_subplots(
        rows=numRows,
        cols=numCols,
        shared_xaxes="all",
        specs=[
            [{"colspan": int(numCols / 2), "rowspan": 2, "r": 0.2 / numCols, "b": 0.3 / numRows}]
            + [None] * (int((numCols - 2) / 2))
            + [{"colspan": int(numCols / 2), "rowspan": 2, "l": 0.2 / numCols, "b": 0.3 / numRows}]
            + [None] * (int((numCols - 2) / 2))
        ]
        + [[None] * numCols]
        + [[{}] * numCols] * (numRows - 2),
        vertical_spacing=0.15 / numRows,
        horizontal_spacing=0.10 / numCols,
    )

    minScore = -1.0
    maxScore = 0.8
    rowIter, colIter = 3, 1
    for elem in dataset:
        if colIter % (numCols + 1) == 0:
            rowIter += 1
            colIter = 1
        fig.add_trace(
            go.Histogram(
                xbins=dict(size=_BIN_SIZE),
                x=elem["df"][elem["df"]["inDomain"] == False]["score"],
                opacity=_HIST_OPACITY,
                marker=dict(color=_OUTSIDE_HIST_COLOR),
                showlegend=False,
                bingroup=str(rowIter) + str(colIter),
            ),
            row=rowIter,
            col=colIter,
        )
        fig.add_trace(
            go.Histogram(
                xbins=dict(size=_BIN_SIZE),
                x=elem["df"][elem["df"]["inDomain"] == True]["score"],
                opacity=_HIST_OPACITY,
                marker=dict(color=_INSIDE_HIST_COLOR),
                showlegend=False,
                bingroup=str(rowIter) + str(colIter),
            ),
            row=rowIter,
            col=colIter,
        )
        for thresh in elem["thresholds"]:
            fig.add_vline(
                x=thresh,
                line=dict(width=1, dash="dot"),
                row=rowIter,
                col=colIter,
            )
        fig.add_annotation(
            text=f"<i>{elem['gene']['symbol']} ({elem['gene']['nucleotide_id']})</i>",
            x=0,
            y=1,
            xref="x domain",
            yref="y domain",
            xanchor="left",
            yanchor="bottom",
            font=dict(size=10, color="gray"),
            showarrow=False,
            row=rowIter,
            col=colIter,
        )

        fig.update_xaxes(
            row=rowIter,
            col=colIter,
            range=[minScore, maxScore],
            showgrid=False,
            showticklabels=False,
            **_plotOutlineParams,
        )
        fig.update_yaxes(
            row=rowIter, col=colIter, showgrid=False, showticklabels=False, **_plotOutlineParams
        )

        colIter += 1

    make_histogram_adjacent_correlation_plot(
        dataset,
        fig=fig,
        col1=1,
        col2=int(numCols / 2) + 1,
        minScore=minScore,
        maxScore=maxScore,
        overwriteCorrLimits=True,
    )

    fig.update_layout(
        template="plotly_white",
        barmode="stack",
        width=1600,
        height=200 * (numRows + 1),
        showlegend=False,
        title=dict(text=title, font=titleFont),
    )

    with open(outFilename + ".html", "w") as f:
        f.write(fig.to_html())
    fig.write_image(outFilename + ".png", scale=2)


@click.group()
@click.help_option()
@click.pass_context
def main(ctx):
    ctx.obj = {}


@main.command()
@click.argument("isoform", type=str)
@click.option("--split-hist", "splitHist", is_flag=True, default=False)
@click.option("--with-variants", "withVariants", is_flag=True, default=False)
@click.pass_context
def domain_figure(ctx, isoform, splitHist, withVariants):
    ctx.obj["db"] = create_engine(
        f"mysql+mysqlconnector://{os.environ('USERNAME_X14')}:{quote_plus(os.environ('PASSWORD_X14'))}@usav1svsqlt14/statistical_genetics"
    )
    logging.info("Getting transcript info...")
    gene = get_transcript_info(isoform)
    logging.info(f"Getting BayesDel scores for {gene['symbol']}...")
    scores = get_chromosome_bayesdel_scores(gene, ctx.obj["db"])
    logging.info("Making plot...")
    if not withVariants:
        make_plot(
            *get_bayesdel_thresholds(gene, ctx.obj["db"]),
            gene=gene,
            scores=scores,
            stackHistogram=splitHist,
        )
    else:
        make_plot_with_variants(
            *get_bayesdel_thresholds(gene, ctx.obj["db"]), gene=gene, scores=scores
        )


@main.command()
@click.option("-f", "--isoforms", "isoformFile", type=str)
@click.option("-c", "--cache-file", "cacheFile")
@click.option("--cache/--no-cache", "cacheIntermediate", default=True)
@click.option("-o", "--output", "outFilename")
@click.option("--title", "title", type=str, default="All Genes")
@click.pass_context
def histogram_matrix(ctx, isoformFile, cacheFile, cacheIntermediate, outFilename, title):
    if not cacheFile:
        ctx.obj["db"] = create_engine(
            f"mysql+mysqlconnector://{os.environ('USERNAME_X14')}:{quote_plus(os.environ('PASSWORD_X14'))}@usav1svsqlt14/statistical_genetics"
        )
        data = []
        isoforms = [x.strip("\n") for x in open(isoformFile)]
        totIsoforms = len(isoforms)
        for index, isoform in enumerate(isoforms):
            logging.info(f"{index}/{totIsoforms} {isoform}")
            try:
                gene = get_transcript_info(isoform)
                data.append(
                    {
                        "gene": gene,
                        "scores": get_chromosome_bayesdel_scores(gene, ctx.obj["db"]),
                        "thresholds": get_bayesdel_thresholds(gene, ctx.obj["db"]),
                    }
                )
            except Exception:
                logging.warning(f"WARNING: Failed for {isoform}")
        if cacheIntermediate:
            pickle.dump(data, open("intermediate.pickle", "wb"))
    else:
        data = pickle.load(open(cacheFile, "rb"))
    make_histogram_matrix(data, title, outFilename)


if __name__ == "__main__":
    main()
