import os
from Bio import PDB
from sys import argv
from pathlib import Path
from dotenv import load_dotenv
from urllib.parse import quote_plus
from sqlalchemy import text, create_engine
from pandas import DataFrame
from collections import Counter

load_dotenv(".env")

engine = create_engine(
    f"mysql+pymysql://{os.getenv('T70_USERNAME')}:{quote_plus(os.getenv('T70_PASSWORD'))}@usav1svsqlt70/black_ops"
)
query = text(
    """SELECT masv3.sp_name,
            pmsv3.seq_in_align
    FROM protein_multialign_seq_v3 pmsv3
    JOIN multiple_alignment_species_v3 masv3 on pmsv3.sp_abbr = masv3.sp_abbr
    WHERE pmsv3.nucleotide_id = :isoform"""
)

with engine.connect() as connection:
    result = connection.execute(query, {"isoform": argv[1]}).fetchall()
humanSeq = next(x[1].replace("Z\r", "") for x in result if x[0] == "Human")

matrix = {}
for i, elem in enumerate(zip(*[x[1].replace("Z\r", "") for x in result])):
    matrix[i + 1] = Counter(elem)
matrix = DataFrame.from_dict(matrix).transpose().fillna(0).drop("-", axis=1)
matrix = matrix.div(matrix.sum(axis=1), axis=0)
seqIds = [matrix.iloc[i][humanSeq[i]] for i in range(0, len(humanSeq))]

structFile = Path(argv[2])
struct = PDB.PDBParser(QUIET=True).get_structure("struct", structFile)
for atom in struct.child_list[0].child_dict["A"].get_atoms():
    if not atom.parent.id[0].startswith("H_"):
        atom.set_bfactor(seqIds[atom.parent.id[1] - 1])
io = PDB.PDBIO()
io.set_structure(struct)
io.save(f"{structFile.stem}_w-conservation.pdb")