#!/usr/bin/env python

import sys
import click
from os import getuid
from pwd import getpwuid
from datetime import datetime
from shlex import split as shlexSplit

from terminaltables import AsciiTable
import xmltodict
import subprocess
import colored
from collections import defaultdict, OrderedDict, Counter

_TITLE_DATE_FMT = "%H:%M %m/%d"
_EMPTY_CELL_CHAR = " "
_HEADERS = [
    "",
    "Job ID",
    "Name",
    "Queue",
    "Work Dir",
    "Start Time",
    "Run Time",
    "CPUs",
    "State",
    "Depend.",
]
_RUN_CODES = ("r", "t", "Rr", "Rt")
_PEND_CODES = ("qw",)
_HARD_PEND_CODES = ("hqw", "hRwq")
_SUSPEND_CODES = (
    "s",
    "ts",
    "S",
    "tS",
    "T",
    "tT",
    "Rs",
    "Rts",
    "RS",
    "RtS",
    "RT",
    "RtT",
)
_ERROR_CODES = ("Eqw", "Ehqw", "EhRqw")

_DEFAULT_STYLE = ""
_ERROR_STYLE = colored.fg("red") + colored.attr("bold")
_PEND_STYLE = colored.fg("yellow")
_HARD_PEND_STYLE = colored.fg("yellow_4a")
_SUSPEND_STYLE = colored.fg("grey_50")

_STATE_COLOR_MAP = defaultdict(
    lambda: colored.fg("white"),
    {
        **{code: _DEFAULT_STYLE for code in _RUN_CODES},
        **{code: _PEND_STYLE for code in _PEND_CODES},
        **{code: _HARD_PEND_STYLE for code in _HARD_PEND_CODES},
        **{code: _ERROR_STYLE for code in _ERROR_CODES},
        **{code: _SUSPEND_STYLE for code in _SUSPEND_CODES},
    },
)


def get_runtime(job) -> str:
    timeDiff = datetime.today() - datetime.fromtimestamp(int(job["JB_submission_time"]))
    minutes = timeDiff.seconds / 60
    hours = minutes / 60
    if timeDiff.days >= 1:
        return f"{timeDiff.days}.{str(hours/24)[2]} days"
    elif hours >= 0.25:
        return f"{hours:.1f} hrs"
    elif minutes >= 1:
        return f"{minutes:.1f} min"
    else:
        return f"{timeDiff.seconds:.1f} sec"


def get_queue(jobInfo) -> str:
    try:
        return jobInfo["JB_hard_queue_list"]["destin_ident_list"]["QR_name"]
    except (KeyError, StopIteration):
        return _EMPTY_CELL_CHAR


def title(jobs) -> str:
    jobTypeStr = "/".join(
        [
            colored.stylize(value, _STATE_COLOR_MAP[key])
            for key, value in Counter(list(zip(*jobs))[1]).items()
        ]
    )
    return f" Total jobs: {len(jobs)} [{jobTypeStr}] "


def get_dependencies(jobInfo) -> str:
    try:
        return ", ".join(
            [x for x in jobInfo["JB_jid_predecessor_list"]["job_predecessors"].values()]
        )
    except KeyError:
        return _EMPTY_CELL_CHAR
    except AttributeError:
        return "XX"


def get_cwd(cwd) -> str:
    homeDir = getpwuid(getuid())[5].split("/")
    workDir = cwd.split("/")
    for index, direc in enumerate(homeDir):
        if direc != workDir[index]:
            return cwd
    return "/".join(["~"] + workDir[len(homeDir) :])


def make_table(jobs, jobInfo):
    table = [_HEADERS]
    index = 1
    for jobID, jobState in jobs:
        oneJob = jobInfo[jobID]
        try:
            table.append(
                [
                    index,
                    oneJob["JB_job_number"],
                    oneJob["JB_job_name"],
                    get_queue(oneJob),
                    get_cwd(oneJob["JB_cwd"]),
                    datetime.strftime(
                        datetime.fromtimestamp(int(oneJob["JB_submission_time"])),
                        _TITLE_DATE_FMT,
                    ),
                    get_runtime(oneJob),
                    oneJob["JB_pe_range"]["ranges"]["RN_max"],
                    jobState,
                    get_dependencies(oneJob),
                ]
            )
            table[-1] = [colored.stylize(x, _STATE_COLOR_MAP[jobState]) for x in table[-1]]
            index += 1
        except KeyError as e:
            print(e)
            continue
    if not bool(table):
        table = [[_EMPTY_CELL_CHAR] * 7]
    table = AsciiTable(table, title=title(jobs))
    table.justify_columns = {i: "left" for i in range(0, len(_HEADERS))}
    return table.table


def make_summary(jobs, jobInfo) -> str:
    numJobTypes = Counter(list(zip(*jobs))[1])
    tables = {
        key: {"description": "", "header": [], "body": []} for key in ("jobType", "resources")
    }
    tables["jobType"]["body"] = [
        [colored.stylize(key, _STATE_COLOR_MAP[key]), value] for key, value in numJobTypes.items()
    ]
    tables["jobType"]["header"] = [["Job Type", "Number"]]
    tables["jobType"][
        "description"
    ] = f"Total number of jobs: {str(sum(list(zip(*tables['jobType']['body']))[1]))}"

    runningJobs = [x[0] for x in jobs if x[1] == "r"]
    tables["resources"]["header"] = [["Resource", "Utilization"]]
    tables["resources"]["body"] = [
        [
            "CPUs",
            sum(
                [
                    int(value["JB_pe_range"]["ranges"]["RN_max"])
                    for key, value in jobInfo.items()
                    if key in runningJobs
                ]
            ),
        ]
    ]
    outStr = ""
    for table in tables.values():
        tmpTable = AsciiTable(table["header"] + table["body"])
        tmpTable.justify_columns = {i: "left" for i in range(0, len(table["header"]))}
        outStr += table["description"] + "\n" + tmpTable.table
    return outStr


@click.command()
@click.option(
    "-s",
    "--summary",
    "printSum",
    is_flag=True,
    help="Print a quick summary of resources utilized by the user.",
)
@click.option("-u", "--user", "username", type=str, default=None)
def qstat(printSum, username):
    """
    Runs `qstat` for the current user, and `qstat -j` for all jobs found, and outputs useful information in a nicely formatted table.
    """

    _defaultTableType = make_table
    if printSum:
        _defaultTableType = make_summary
    cmd = "qstat -xml"
    if username:
        cmd = f"qstat -u {username} -xml"
    proc = subprocess.run(shlexSplit(cmd), stdout=subprocess.PIPE)
    try:
        jobsDict = xmltodict.parse(proc.stdout.decode())["job_info"]
    except TypeError:
        print(proc.stdout.decode())
        sys.exit()
    jobs = []
    for jobType in ("queue_info", "job_info"):
        if jobsDict[jobType] is None:
            jobsDict[jobType] = {}
        if isinstance(jobsDict[jobType].get("job_list"), list):
            jobs += [(x["JB_job_number"], x["state"]) for x in jobsDict[jobType]["job_list"]]
        elif isinstance(jobsDict[jobType].get("job_list"), OrderedDict):
            job = jobsDict[jobType]["job_list"]
            jobs += [(job["JB_job_number"], job["state"])]
    jobInfo = {}
    for job in jobs:
        proc = subprocess.run(shlexSplit(f"qstat -xml -j {job[0]}"), stdout=subprocess.PIPE)
        jobInfo[job[0]] = xmltodict.parse(proc.stdout.decode())["detailed_job_info"]["djob_info"][
            "element"
        ]
    if jobs:
        print(_defaultTableType(jobs, jobInfo))
    else:
        print("There are no currently running jobs.")


if __name__ == "__main__":
    qstat()
