#!/usr/bin/env python

"""
This script uploads/downloads JSON and PowerPoint reports from the strappy-reports API.
"""

import re
import time
import requests
import click

baseURL = "http://{host}:{port}/rest/reports"
uploadURL = "{baseURL}/upload_file"
downloadURL = "{baseURL}/{reportID}.pptx"


def _base_url(host, port) -> str:
    return baseURL.format(host=host, port=port)


def upload_url(host, port) -> str:
    return uploadURL.format(baseURL=_base_url(host, port))


def download_url(host, port, reportID) -> str:
    return downloadURL.format(baseURL=_base_url(host, port), reportID=reportID)


@click.command()
@click.option("-r", "--report-file", "reportFile", type=str)
@click.option("-i", "--report-id", "reportID", type=str)
@click.option("-o", "--output", "outFileName", type=str, default="report.pptx")
@click.option("--host", "host", type=str, default="dockert01")
@click.option("--port", "port", type=str, default="8191")
def main(reportFile, reportID, outFileName, host, port):
    if not (reportFile or reportID):
        print("Please provide a report file to upload or a report ID to download")
        return
    if reportFile:
        response = requests.post(
            upload_url(host, port), files={"reportFile": open(reportFile, "rb")}
        )
        reportID = response.headers["location"].split("/")[-1]
        print(f"Report successfully uploaded, with report ID '{reportID}'.")
    try:
        with requests.get(download_url(host, port, reportID)) as report:
            report.raise_for_status()
            filename = re.findall("filename=(.+)", report.headers.get("content-disposition"))
            if filename:
                filename = filename[0]
            else:
                filename = "report.pptx"
            with open(filename, "wb") as outFile:
                outFile.write(report.content)
            print(f"Successfully downloaded report to {filename}")
    except (TypeError, requests.exceptions.HTTPError) as e:
        print("Error downloading PowerPoint report")
        print(e)


if __name__ == "__main__":
    main()
