"""
Short script to request information from bioapi for a set of variants.
Matthew Varga - 09/2022

Input: CSV containing two columns, gene name and HGVS-formatted c-variant (including leading 'c.')
    Example:
        MLH1,c.115T>C
        DARS2,c.396+2T>G
Output: CSV containing the original columns and the bioapi fields contained in `requestFields`, with
headers

Usage: python ./fetch_bioapi_data.py $input $output
"""

import requests
from pathlib import Path
from urllib.parse import quote
from sys import argv
from tqdm import trange, tqdm
from pandas import read_csv


requestFields = {
    "protein_alteration": "Protein Alteration",
    "nucleotide_id": "Nucleotide ID",
    "chromosome": "Chromosome",
    "genomic_position": "Genomic Position(s)",
    "genomic_vcf_position": "Genomic Position (VCF)",
    "genomic_vcf_ref_nt": "Genomic Reference Nucleotide (VCF)",
    "genomic_vcf_alt_nt": "Genomic Alternative Nucleotide (VCF)",
    "nucleotide_type": "Nucleotide Variant Type",
    "protein_type": "Protein Variant Type",
    "sub_category": "Variant Subtype",
    "exon_nums": "Impacted Exons",
    "cds_num": "Impacted CDS",
}


def clean_genomic_position(value):
    if isinstance(value, str):
        return value.replace(",", "-")
    else:
        return value


def get_bioapi_data(symbol, c_variant):
    try:
        response = requests.get(
            f"http://api/api/alterations.json?alterations={symbol}+{quote(c_variant)}"
        )
        response.raise_for_status()
        response = response.json()["alterations"][0]
        return [response.get(key) for key in requestFields.keys()]
    except Exception as e:
        tqdm.write(f"Error for {symbol} {c_variant}: {e.__class__.__name__}('{e}')")
    return [None] * len(requestFields.keys())


if __name__ == "__main__":
    inFilename = Path(argv[1])
    if len(argv) == 3:
        outFilename = Path(argv[2])
    else:
        outFilename = Path(inFilename.stem + "_with_data.csv")
    print(f"Reading file '{inFilename}' and writing results to '{outFilename}")
    variants = read_csv(argv[1], header=None, names=["symbol", "c_variant"])
    data = []
    try:
        for i in trange(0, len(variants)):
            data.append(get_bioapi_data(variants.iloc[i]["symbol"], variants.iloc[i]["c_variant"]))
    except KeyboardInterrupt:
        print("Interrupted! Writing partial results")
        data.extend([[None] * len(requestFields.keys())] * (len(variants) - len(data)))
    variants[list(requestFields.keys())] = data
    variants["genomic_position"] = variants["genomic_position"].apply(clean_genomic_position)
    variants = variants.rename(
        columns={"symbol": "Gene Name", "c_variant": "Nucleotide Alteration", **requestFields}
    )
    variants.to_csv(outFilename, index=False)
    print("Finished!")
