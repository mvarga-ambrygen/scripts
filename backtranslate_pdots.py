"""
Script to backtranslate a list of pdots to all possible SNVs which result in that pdot.
Takes a file of pdots, one on each line, with 'p.' prefix included. Outputs list of pdot, cdot
pairs to "variants_with_cdots.csv"

Usage:
    python backtranslate_pdots.py $isoform $inputFile
"""
import sys
import requests
from Bio.Seq import Seq
from mutalyzer_backtranslate import BackTranslate


translator = BackTranslate()


def get_cdots(pos: int, alt: str):
    cdots = []
    if alt == "X":
        alt = "*`"
    changes = translator.with_dna(codons[pos - 1], alt)
    for offset, alleles in changes.items():
        for allele in alleles:
            cPos = (pos * 3) - 2 + offset
            cdots.append(f"c.{cPos}{allele[0]}>{allele[1]}")
    return cdots


if __name__ == "__main__":
    isoform = sys.argv[1]
    response = requests.get(f"http://pyapi/api/v1.0/info/transcript?nucleotide_id={isoform}").json()["api_data"][0]

    seq = Seq(response["nucleotide_seq"][response["tl_start_site"] - 1:response["tl_stop_site"]].upper())

    transSeq = seq.translate(to_stop=True)
    assert str(transSeq) == response["protein_seq"].upper()

    codons = [str(seq[i:i+3]) for i in range(0, len(seq), 3)]

    variants = {line.rstrip(): [] for line in open(sys.argv[2])}
    for variant in variants.keys():
        variants[variant].extend(get_cdots(int(variant[1:-1]), variant[-1]))
    with open("variants_with_cdots.csv", "w") as f:
        f.write("p_variant,c_variant\n")
        for pDot, cDots in variants.items():
            if len(cDots) == 0:
                continue
            else:
                for cDot in cDots:
                    f.write(f"p.{pDot},{cDot}\n")
