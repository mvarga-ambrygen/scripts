import os
import logging
from py.xml import html

def pytest_html_report_title(report):
    report.title = "EviPred API Validation Report"

def pytest_configure(config):
    config._metadata["API Host"] = os.environ.get("TAVERN_EVIPRED_HOST")

def pytest_tavern_beta_after_every_response(expected, response):
    try:
        logging.info(f"Returned with prediction '{response.json()['alterations'][0]['prediction']}'")
    except Exception:
        pass
