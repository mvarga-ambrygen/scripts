import re
from requests import Response
from typing import List

def validate_prediction(response: Response, prediction: str) -> bool:
    assert response.json()["alterations"][0]["prediction"] == prediction
    return True

def validate_node_id(response: Response, node_id: str) -> bool:
    assert response.json()["alterations"][0]["path"][-2]["id"] == node_id
    return True

def validate_db_check(response: Response, dbTypes: List) -> bool:
    finalDecision = response.json()["alterations"][0]["path"][-2]
    assert bool(set(dbTypes).intersection(set(finalDecision["sources"]))) == True
    return True

def validate_node_class(response: Response, nodeClass: str) -> bool:
    finalDecision = response.json()["alterations"][0]["path"][-2]
    testIsTrue = False
    for test in finalDecision["test"]:
        if test["class"] == nodeClass and test["result"] == True:
            testIsTrue = True
            break
    assert testIsTrue == True
    return True

def validate_specific_path(response: Response, nodePath: List) -> bool:
    ...

def validate_af_type(response: Response, afType: str) -> bool:
    finalDecision = response.json()["alterations"][0]["path"][-2]
    if afType == "allele":
        assert re.search("\(\d+\)", finalDecision["resultText"]) != None
    else:
        assert re.search("\(0\.\d+\)", finalDecision["resultText"]) != None
    return True

def validate_node_error(response: Response, nodeId: str, errorMsg: str) -> bool:
    targetNode = next(node for node in response.json()["alterations"][0]["path"] if node.get("id") == nodeId)
    assert targetNode["error"] == errorMsg
    return True

def validate_node_statuses(response: Response, statuses: List[List]):
    for node in statuses:
        assert next(x for x in response.json()["alterations"][0]["path"] if x.get("id") == node[0])["success"] == node[1]
    return True

def validate_node_results(response: Response, results: List[List]):
    for node in results:
        assert next(x for x in response.json()["alterations"][0]["path"] if x.get("id") == node[0])["result"] == node[1]
    return True
